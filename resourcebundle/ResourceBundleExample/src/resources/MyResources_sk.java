package resources;
import java.util.ListResourceBundle;
//This is our property file which extends ListResourceBundle
public class MyResources_sk extends ListResourceBundle {

	//this methods gets the data from the resources object which we have initialized below
    @Override
    protected Object[][] getContents() {
        
        return resources;
    }

    //Here we are initializng our properties which will be accessed from main class using getBundle
    private final Object[][] resources = {  
        
            { "Capital", "Bratislava" },       
            { "Area", 49035 },
            { "Currency", "EUR" },
    };
}