package com.example;

import java.util.Locale;
import java.util.ResourceBundle;
public class Main {
	

	    public static void main(String[] args) {
	        
	        Locale sk_loc = new Locale("sk", "SK");  //We have two locale in properties and this is 1
	        ResourceBundle bundle = 
	            ResourceBundle.getBundle("resources.MyResources", sk_loc); //we get the bundle for the currently used locale and load data from property files

	        System.out.println("Capital: " + bundle.getObject("Capital")); //Here we are getting data from the properties and printing it
	        System.out.println("Area: " + bundle.getObject("Area"));
	        System.out.println("Currency: " + bundle.getObject("Currency"));
	        
	        System.out.println();
	        
	        Locale cz_loc = new Locale("cs", "CZ"); //We have two locale in properties and this is 2
	        ResourceBundle bundle2 = 
	            ResourceBundle.getBundle("resources.MyResources", cz_loc);

	        System.out.println("Capital: " + bundle2.getObject("Capital"));
	        System.out.println("Area: " + bundle2.getObject("Area"));
	        System.out.println("Currency: " + bundle2.getObject("Currency"));      
	    }
	}