import java.text.NumberFormat;
import java.util.Locale;

public class FormatNum {
	public static void main(String[] args) {
		
		Locale currentLocale = Locale.getDefault();
		
		System.out.println(currentLocale);
		
		Locale localeUS = Locale.US;
		var localeDE = Locale.GERMANY;
		var LocaleCustom = new Locale("en", "SG");

		NumberFormat usGen = NumberFormat.getInstance(localeUS);
		var usNum = NumberFormat.getNumberInstance(localeUS);
		var usCur = NumberFormat.getCurrencyInstance(localeUS);

		NumberFormat deGen = NumberFormat.getInstance(localeDE);
		
		System.out.println(usGen.format(1_000_000));
		System.out.println(deGen.format(1_000_000));
		
	}
}
