import java.time.LocalDateTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

public class ParseDateFormatter {
    public static void main(String[] args) {

        var italy = new Locale("it", "IT");
        var us = new Locale("en", "US");
        var dt = LocalDateTime.of(2020, Month.OCTOBER, 20, 15, 12, 34);
        var formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT).withLocale(italy);
        var formatter1 = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT).withLocale(us);
        
        System.out.println("With default dateTime formatter " + dt);
        System.out.println("With localised Italy dateTime formatter " + dt.format(formatter));
        System.out.println("With localised US dateTime formatter " + dt.format(formatter1));

        
    
    }

    
}
