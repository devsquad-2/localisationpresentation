import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class ParseNum {
    //NumberFormat.parse method will convert a string to a
    // structured objecct or primitive value while taking into account the locale
    public static void main(String[] args) {
        String ticketPrice = "40.45";

        var en = NumberFormat.getInstance(Locale.US);
        var fr = NumberFormat.getInstance(Locale.FRANCE);
        try {

            System.out.println(en.parse(ticketPrice));
            System.out.println(fr.parse(ticketPrice));
            //The parse method will throw a ParseException that must be handled
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    

}
