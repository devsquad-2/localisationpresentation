import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

public class ParseCurrency {
    public static void main(String[] args) {
        String income = "$92,807.99";
        var defLoc = NumberFormat.getCurrencyInstance();
        //Get currency format for the current default format locale
        System.out.println(Locale.getDefault());
        var usLoc = NumberFormat.getCurrencyInstance(Locale.US);
        //Get currency format for the specified locale.
        try {

            double defValue = (Double) defLoc.parse(income);
            double usValue = (Double) usLoc.parse(income);
            System.out.println("Value of income in default locale: " + defValue);
            System.out.println("Value of income in US locale: " + usValue);

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}