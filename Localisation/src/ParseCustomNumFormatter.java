import java.text.DecimalFormat;
import java.text.NumberFormat;

public class ParseCustomNumFormatter {
    public static void main(String[] args) {
        
        int num = 1234567;
        NumberFormat hashTagForm = new DecimalFormat("###,###,###");
        NumberFormat zeroForm = new DecimalFormat("000,000,000");
        System.out.println(hashTagForm.format(num));
        System.out.println(zeroForm.format(num));

    }
}